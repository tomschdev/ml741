import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Activation
import numpy as np
from sklearn.datasets import load_iris, load_wine, load_breast_cancer
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.utils import to_categorical
import numpy as np
import math
import sklearn
from sklearn.model_selection import GridSearchCV
import pandas as pd
from sklearn import preprocessing
from sklearn.metrics import f1_score
import time
from tqdm import tqdm

#TODO
# 1 output for breast-cancer?

#CHECKLIST
# - FILE NAMEs in vers!
# - beta values in betas
# - dataset
# - penalty
# - mode
# epochs
# rnds

VERS = ["data/krak_sasla_S1-wine.txt", "data/krak_sasla_S2-wine.txt", "data/krak_sasla_S3-wine.txt"]
BETAS = [0.5, 0.7, 0.9]

# VERSION = "data/krak_sasla_PSGD-bc.txt"

DATA = "wine"
UNITS = [0,0,0]
PEN = 0

if DATA == 'iris':
    UNITS = [4,5,3] #iris
    PEN = 0.0001

elif DATA == 'wine':
    UNITS = [12,8,3]
    PEN = 0.00001

elif DATA == 'breast-cancer':
    UNITS = [8,5,2]
    PEN = 0.0001

MODE = "SASLA"
# BETA = 0.9
EPOCH = 200
RND = 5



  

def calc_recall(y_true, y_pred):
    true_positives = tf.keras.backend.sum(tf.keras.backend.round(tf.keras.backend.clip(y_true * y_pred, 0, 1)))
    possible_positives = tf.keras.backend.sum(tf.keras.backend.round(tf.keras.backend.clip(y_true, 0, 1)))
    recall = true_positives / (possible_positives + tf.keras.backend.epsilon())
    return recall

def calc_precision(y_true, y_pred):
    true_positives = tf.keras.backend.sum(tf.keras.backend.round(tf.keras.backend.clip(y_true * y_pred, 0, 1)))
    predicted_positives = tf.keras.backend.sum(tf.keras.backend.round(tf.keras.backend.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + tf.keras.backend.epsilon())
    return precision

def f1(y_true, y_pred):
    precision = calc_precision(y_true, y_pred)
    recall = calc_recall(y_true, y_pred)
    return 2*((precision*recall)/(precision+recall+tf.keras.backend.epsilon()))


def fetch_data(key):
    if key == "iris":
        iris_data = load_iris() # load the iris dataset
        X = iris_data.data
        #normalize features
        X = preprocessing.normalize(X)
        y = iris_data.target.reshape(-1, 1) # Convert data to a single column

    elif key == "wine":
        wine_data = load_wine()
        X = wine_data.data
        X = np.hstack((X[:, :8], X[:, 9:]))
        y = wine_data.target.reshape(-1, 1)
        X = preprocessing.normalize(X)
        #remove least relevant feature: nonflavanoid_phenols: now 12 descriptive features


    elif key == "breast-cancer":
        bc_data = load_breast_cancer()
        y = bc_data.target.reshape(-1, 1)
        df = pd.DataFrame(bc_data.data, columns=bc_data.feature_names)
        print(bc_data.feature_names)
        dfc = pd.DataFrame({
            "radius_mean": df["mean radius"],
            "perimeter_mean": df["mean perimeter"],
            "area_mean": df["mean area"],
            "concave_points_mean": df["mean concave points"],
            "texture_mean": df["mean texture"],
            "smoothness_mean": df["mean smoothness"],
            "compactness_mean": df["mean compactness"],
            "concavity_mean": df["mean concavity"],
        })
        X = np.asarray(dfc.values)
        X = preprocessing.normalize(X)
    return X, y
        
def return_NN(i_units, h_units, o_units, penalty):
    model = Sequential()
    model.add(Dense(h_units, input_dim=i_units, kernel_regularizer=tf.keras.regularizers.l2(penalty))) 
    model.add(Dense(o_units, activation='softmax', kernel_regularizer=tf.keras.regularizers.l2(penalty))) #TODO reg
    sgd = SGD(learning_rate=0.1, decay=1e-6, momentum=0.9, nesterov=True)
    model.compile(optimizer=sgd, loss='categorical_crossentropy', metrics=['accuracy', f1])
    return model

def get_weights(NN):
    weight_d = {}
    bias_d = {}
    
    for layerNum, layer in enumerate(NN.layers):
        weights = layer.get_weights()[0]
        biases = layer.get_weights()[1]
        weight_d[layerNum] = np.asarray(weights)
        bias_d[layerNum] = np.asarray(biases)  

        # print(f'from LAYER {layerNum}')
        # print(f'to LAYER {layerNum+1}')
        # for fromNeuronNum, wgt in enumerate(weights):
            # print(f'\tweight from neuron {fromNeuronNum}')
            # for toNeuronNum, wgt2 in enumerate(wgt):
                # print(f'\t\tto neuron {toNeuronNum}:', end=" ")
                # print(f'{wgt2}')
        # for toNeuronNum, bias in enumerate(biases):
            # print(f'\tbias of unit {toNeuronNum}, layer {layerNum+1}: {bias}')

    return weight_d, bias_d
"""
END UTILITIES
"""


def grid_search_penalty(X, y, mags, epochs):
    loss_d = {}

    train_X, test_X, train_y, test_y = train_test_split(X, y, test_size=0.20)

    for m in mags:
        NN = return_NN(UNITS[0], UNITS[1], UNITS[2], m)
        NN.fit(train_X, train_y, epochs=epochs)
        loss_d[m] = (NN.evaluate(test_X, test_y))[0]
    
    best = 999
    best_key = 999
    for key, val in loss_d.items():
        if val < best:
            best_key = key
            best = val 
        print(f'{key}: {val}')

    return best

def compute_activations(inp, w, b, af=None):
    wT = np.transpose(w)
    smtn = np.matmul(wT, inp)
    out = np.add(smtn, b)
    
    if af == "softmax":
        out = tf.nn.softmax(out)
    
    return out

def compute_informativeness_per_pattern(patterns, weight_d, bias_d):
    """
    patterns is numpy array
    """
    inform_p = np.zeros(patterns.shape[0])
    
    flw = weight_d[0]
    slw = weight_d[1]
    
    I = UNITS[0]
    H = UNITS[1] #TODO bias vectors as well
    K = UNITS[2]
    
    #compute sensitvity pattern for each pattern and store in p
    # print(f"computing sensitvity matrix of dimension {K} x {I}")
    # print("rows = NN number of outputs")
    # print("columns = NN number of inputs")
    # print("therefore have rows of each output unit sensitivity to each input unit")
    
    for p in range(patterns.shape[0]):
        pattern = patterns[p]
        
        h_act = compute_activations(pattern, weight_d[0], bias_d[0]) 
        o_act = compute_activations(h_act, weight_d[1], bias_d[1], af="softmax")
        
        smat = np.zeros((K,I))
        
        #VERIFY THESE WITH PRINTS (right weights in right places)
        for k in range(K):
            for i in range(I):
                hcalc = 0
                for j in range(H): #TODO j-1?
                    hcalc += slw[j][k]*(1-h_act[j])*h_act[j]*flw[i][j]
                smat[k][i] = (1-o_act[k])*o_act[k]*hcalc

        # TODO what to do with bias (page 1008 sasla) 
        smat = np.concatenate((smat, bias_d[1].reshape(-1,1)), axis=1)          

        #compute sensitivity vector
        svec = np.zeros(K)
        for k in range(K):
            # svec[k] = math.sqrt(np.sum(np.square(smat[k])))
            svec[k] = np.sum(np.abs(smat[k]))
        # print("sum norm sensitivity vector")
        # print(svec)

        #compute informativeness
        infp = max(svec)   
        # print("informativeness of pattern:")
        # print(infp)

        #update informativeness for pattern
        inform_p[p] = infp 
        
    return inform_p

def sasla_classify_NN(X, y, epochs, rounds, beta):
    results = pd.DataFrame(columns=['acc_t', 'acc_g', 'p', 'acc_g_best', 'f1_g', 'f1_g_best', 'best_acc_epoch', 'best_acc_at_prsnt', 'cost'])
    with tf.device("/GPU:0"):
        for r in range(rounds):
            NuN = None
            NuN = return_NN(UNITS[0], UNITS[1], UNITS[2], PEN)
            train_X, test_X, train_y, test_y = train_test_split(X, y, test_size=0.20)
            
            print("[ROUND]")
            store_num_patterns = []
            acc_hist= []
            val_acc_hist = []
            val_f1_hist = []
            start = time.time()
            for e in range(epochs):
                if e == 0:
                    history = NuN.fit(train_X, train_y, epochs=1, validation_data=(test_X, test_y))
                    val_acc_hist.extend(history.history["val_accuracy"])
                    acc_hist.extend(history.history["accuracy"])
                    val_f1_hist.extend(history.history["val_f1"])
    
                else:
                    sample_X = train_X[0]
                    label = train_y[0]
                    weight_d, bias_d = get_weights(NuN)
    
                    #verify that computations are correct
                    h_act = compute_activations(sample_X, weight_d[0], bias_d[0]) 
                    o_act = compute_activations(h_act, weight_d[1], bias_d[1], af="softmax")
                    pred_y = NuN.predict(np.array(sample_X).reshape(1,-1))
                    assert np.allclose(pred_y, o_act), "[WARNING] Keras Prediction is off from manual computation mine: {}, keras: {}".format(o_act, pred_y)
    
                    #compute infromativeness for each pattern
                    inform_p = compute_informativeness_per_pattern(np.asarray(train_X), weight_d, bias_d)
                    inform_avg = (1-beta)*np.mean(inform_p)
    
                    #filter candidate set based on informativeness measures
                    Dt_X = train_X[inform_p > inform_avg] 
                    Dt_y = train_y[inform_p > inform_avg]
                    store_num_patterns.append(int(Dt_y.shape[0]))
                    print("D_t input shape", Dt_y.shape[0])
    
                    history = NuN.fit(Dt_X, Dt_y, epochs=1, verbose=1, validation_data=(test_X, test_y))
                    val_acc_hist.extend(history.history["val_accuracy"])
                    acc_hist.extend(history.history["accuracy"])
                    val_f1_hist.extend(history.history["val_f1"])
    
            end = time.time()
            cost = end - start
            acc_g_best = max(val_acc_hist)
            f1_g_best = max(val_f1_hist)
            for i, val in enumerate(val_acc_hist):
                if val == acc_g_best:
                    best_acc_epoch = i+1
                    break
                
            best_num_patterns = sum(store_num_patterns[:best_acc_epoch])
    
            result_round = None
            result_round = [acc_hist[len(acc_hist)-1], val_acc_hist[len(val_acc_hist)-1], float(val_acc_hist[len(val_acc_hist)-1]/acc_hist[len(acc_hist)-1]), acc_g_best, val_f1_hist[len   (val_f1_hist)-1], f1_g_best, best_acc_epoch, best_num_patterns, cost]
            results.loc[r] = result_round
            del NuN
            tf.keras.backend.clear_session()

    print(results)
    return results

    

    # print("validation loss: {}".format(res[0]))
    # print("validation accuracy: {}".format(res[1]))
    # for y, yp in zip(test_y, pred_y):
        # print("test_y: {}".format(y))
        # print("pred_y: {}\n".format(yp)) 
    
    # for i, w in weight_d.items():
            # print(f'layer {i}:\n{w}')
        # for i, b in bias_d.items():
            # print(f'layer {i}:\n{b}')
    
    # print("label")
    # print(label)
    # print("keras pred_y")
    # print(pred_y)
    # print("manual")
    # print(o_act)


def passive_classify_NN(X, y, epochs, rounds):
    results = pd.DataFrame(columns=['acc_t', 'acc_g', 'p', 'acc_g_best', 'f1_g', 'f1_g_best', 'best_acc_epoch', 'best_acc_at_prsnt', 'cost'])
    for r in range(rounds):
        start = time.time()
        NuN = None
        NuN = return_NN(UNITS[0], UNITS[1], UNITS[2], PEN)
        train_X, test_X, train_y, test_y = train_test_split(X, y, test_size=0.20)

        history = None
        history = NuN.fit(train_X, train_y, epochs=epochs, validation_data=(test_X, test_y))
        end = time.time()
        cost = end - start
        val_acc_hist = history.history["val_accuracy"]
        acc_hist = history.history["accuracy"]
        val_f1_hist = history.history["val_f1"]
        print(len(acc_hist))
        print(len(val_acc_hist))
        acc_g_best = max(val_acc_hist)
        f1_g_best = max(val_f1_hist)
        
        for i, val in enumerate(val_acc_hist):
            if val == acc_g_best:
                best_acc_epoch = i+1
                break
        best_num_patterns = best_acc_epoch*(train_y.shape[0])
            
        result_round = [acc_hist[len(acc_hist)-1], val_acc_hist[len(val_acc_hist)-1], float(val_acc_hist[len(val_acc_hist)-1]/acc_hist[len(acc_hist)-1]), acc_g_best, val_f1_hist[len(val_f1_hist)-1], f1_g_best, best_acc_epoch, best_num_patterns, cost]
        results.loc[r] = result_round
        del NuN
        tf.keras.backend.clear_session()
    

    print(results)
    return results
   

def main():
    #prep data
    X, y = fetch_data(DATA)
    encoder = OneHotEncoder(sparse=False)
    y = encoder.fit_transform(y)

    for VERSION, BETA in tqdm(zip(VERS, BETAS)):
        if MODE == "FSL":
            results = passive_classify_NN(X, y, EPOCH, RND)
        elif MODE == "SASLA":
            results = sasla_classify_NN(X, y, EPOCH, RND, BETA)
        elif MODE == "ALUS":
            pass


    fnl = results.describe().fillna("-1")
    fnl = fnl.drop('min')
    fnl = fnl.drop('max')
    fnl = fnl.drop('25%')
    fnl = fnl.drop('50%')
    fnl = fnl.drop('75%')
    print(fnl)
    fnl.to_csv(VERSION, sep="&", float_format='%.2f', header=['acc_t', 'acc_g', 'p', 'acc_g_best', 'f1_g', 'f1_g_best', 'best_acc_epoch', 'best_acc_at_prsnt', 'cost'])

    # grid_search_penalty(train_X, train_y, [0.1, 0.01, 0.001, 0.0001, 0.00001, 0.000001, 0.0000001], epochs=100)
    #retrieve model and train
    # print("SASLA RUN")
    # sasla_classify_NN(NN, train_X, train_y, test_X, test_y, epochs=100, beta=0.5)
    # print("passive RUN")
    # passive_classify_NN(NN, train_X, train_y, test_X, test_y, epochs=100)

if __name__ == '__main__':
    main()
    